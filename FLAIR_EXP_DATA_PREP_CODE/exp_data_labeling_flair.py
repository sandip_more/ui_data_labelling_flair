import tkinter as tk

import pickle
with open ('data_real_around_dates.pkl', "rb") as pickle_off:
  data = pickle.load(pickle_off)
print(f"Total number of records: {len(data)}")
rec_start=0
rec_end=2
records_file_name=str(rec_start)+'_'+str(rec_end)+'.pkl'
data=data[rec_start:rec_end]

txt=None
records=[]
TEXT=None
ENTITY_LIST=[]
# ENTITY_COMPANY=None
# ENTITY_ROLE=None
# ENTITY_LOCATION=None
ENTITY=None
def isBlank (myString):
    if myString and myString.strip():
        #myString is not None AND myString is not empty or blank
        return False
    #myString is None OR myString is empty or blank
    return True
def copy_text(event):
    try:
        global txt
        txt = readOnlyText.get('sel.first', 'sel.last')
    except tk.TclError:
        print("# no selection")        
        return "break"
    except Exception as e:
        print("Error in reading")
        return "break"
    print(f"Selected text: {txt}")
    #got selected text in txt
    if(isBlank(txt)):
        print("Blank selection")
        return "break"
      
    
    return "break"  # prevent class binding to be triggered

def create_record_company(event):
    # print(f"Record created {txt} {event} {event.__dict__}")
    # global TEXT
    global ENTITY_LIST
    # global records

    if(isBlank(txt)):
        return "break"

    ENTITY=(txt,'COMPANY')
    ENTITY_LIST.append(ENTITY)
    
    return "break"

def create_record_location(event):
    # print(f"Record created {txt} {event} {event.__dict__}")
    # global TEXT
    global ENTITY_LIST
    # global records

    if(isBlank(txt)):
        return "break"

    ENTITY=(txt,'LOCATION')
    ENTITY_LIST.append(ENTITY)

    return "break"
def create_record_role(event):
    # print(f"Record created {txt} {event} {event.__dict__}")
    # global TEXT
    global ENTITY_LIST
    # global records

    if(isBlank(txt)):
        return "break"

    ENTITY=(txt,'ROLE')
    ENTITY_LIST.append(ENTITY)
    
    return "break"


 



root = tk.Tk()
readOnlyText = tk.Text(root, fg="dark green")
# counter_label(readOnlyText)
readOnlyText.configure(state='normal',font=("Courier", 16, "italic"))
readOnlyText.place(x=0,y=0,width=1000) 
readOnlyText.bind('<Control-c>', copy_text)
readOnlyText.delete('1.0', tk.END)
readOnlyText.insert('end',data[0]['text'])
TEXT=data[0]['text']

counter = 0
def getNextData():
    global counter
    global TEXT
    global ENTITY_LIST
    global records
    ENTITY_LIST.append((data[counter]['period'],'DATE'))
    records.append([TEXT,ENTITY_LIST])
    ENTITY_LIST=[]
    for r in records:
        print(r)
    import pickle
    with open(records_file_name, 'wb') as fh:
        pickle.dump(records, fh)
    counter += 1
    if(counter>len(data)):
        return 'break'
    readOnlyText.delete('1.0', tk.END)
    try:
        readOnlyText.insert('end',data[counter]['text'])
        TEXT=data[counter]['text']        
    except IndexError:
        pass    
    
def quit(event):
    with open(records_file_name, 'wb') as fh:
        pickle.dump(records, fh)
    root.destroy()




btn_COMPANY = tk.Button(root, text = 'COMPANY')
btn_COMPANY.place(x=1100, y=0)
btn_COMPANY.bind('<Button-1>', create_record_company)

btn_ROLE = tk.Button(root, text = 'ROLE')
btn_ROLE.place(x=1100, y=40)
btn_ROLE.bind('<Button-1>', create_record_role)

btn_LOCATION = tk.Button(root, text = 'LOCATION')
btn_LOCATION.place(x=1100, y=80)
btn_LOCATION.bind('<Button-1>', create_record_location)

btn_NEXT = tk.Button(root, text = 'NEXT',command = getNextData)
btn_NEXT.place(x=1100, y=220)

btn_q = tk.Button(root, text = 'Quit!')
btn_q.place(x=1100, y=460)
btn_q.bind('<Button-1>', quit)
root.mainloop()

# readOnlyText.insert('end',"xx")
